import click
import redis
import math

r = redis.StrictRedis(host='localhost', port=6379, db=0)


class Counter(dict):
    def __missing__(self, key):
        return 0

@click.group()
def mxm():
    pass

def parseSong(data,song_info):
    counts = data.rstrip('\n').split(',')

    if len(counts) > 1:
        song_id = counts[0]

        r.lpush('mxm:songs',song_id)

        counts = counts[2:]
        words_in_song = 0
        for song_count in counts:
            buf = song_count.split(':')

            word = int(buf[0]) - 1
            count = int(buf[1])

            words_in_song += count;
            
            song_info[word] += 1
            r.hset('mxm:' + song_id,word,count)

        r.hset('mxm:wordInSongs',song_id,words_in_song)

@mxm.command()
@click.argument('fileobj',type=click.File('r'))
def load(fileobj):

    if r.exists('mxm:db'):
        click.echo('база данных создана. Перeд загрузкой новой удалите текущую.')
    else:
        r.set('mxm:db','create')

        song = Counter()
        for line in fileobj.readlines():
            
            if line[0] == '#':
                #comment
                continue
            elif line[0] == '%':
                #parse words
                words = reversed(line.lstrip('%').rstrip('\n').split(','))
                
                for word in words:
                    r.lpush('mxm:words',word)
            else:
                #parse songs
                parseSong(line,song)

        for (k,v) in song.items():
            r.hset('mxm:song',k,v)
        echoInfo()

@mxm.command()
def remove():
    keys = r.keys('mxm:*')
    for key in keys:
        r.delete(key)
    #r.flushdb()

def echoInfo():
    click.echo('loaded {} words'.format(r.llen('mxm:words')))
    click.echo('loaded {} songs'.format(r.llen('mxm:songs')))

@mxm.command()
def stats():
    echoInfo()

@mxm.command()
def answer():
    if not r.exists('mxm:db'):
        click.echo('нет загруженных данных. используйте load [file]')
    else:
        max_word = 0
        max_song_id = 0
        max_tf_idf = 0

        number_of_song = r.llen('mxm:songs')

        for song in range(0,number_of_song):
            song_id = r.lindex('mxm:songs',song).decode("utf-8")

            words = r.hkeys('mxm:'+song_id)

            for word in words:

                tf_idf = int(r.hget('mxm:'+song_id,word))/int(r.hget('mxm:wordInSongs',song_id))*math.log(number_of_song/int(r.hget('mxm:song',word)))

                if tf_idf > max_tf_idf:
                    max_tf_idf = tf_idf
                    max_song_id = song_id
                    max_word = word

        click.echo('song {} and word {} have maximal TF-IDF value: {}'
            .format(max_song_id,r.lindex('mxm:words',max_word).decode("utf-8"),max_tf_idf))

if __name__ == '__main__':
    mxm()